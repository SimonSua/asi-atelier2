function handleConnexion() {
	var sUser = document.getElementById("user").value;
	var sPassword = document.getElementById("password").value;
	var oParameter = {
		"username": sUser,
		"password": sPassword
	};
	/*$.ajax({
		type: "POST",
		url: "http://127.0.0.1:8080/addUser",
		data: JSON.stringify({
			username: sUser,
			password: sPassword,
			money: 0,
			cards: [],
			token: ""
		}),
		contentType: "application/json"
	});*/
	$.ajax({
		type: "POST",
		url: "/connexion",
		data: JSON.stringify(oParameter),
		contentType: "application/json",
		success: function(sToken) {
			if(sToken) {
				var dDate = new Date();
				dDate.setMonth(dDate.getMonth() + 1);
				document.cookie = "cardapptoken=" + sToken + ";expires=" + dDate.toUTCString() + "; path=/";
				console.log(document.cookie);
				window.location.href = "cardHome.html";
			} else {
				alert("Informations de connexion erronées");
			}
		}
	});
}
