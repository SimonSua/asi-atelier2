var user;
var currentCardId;
$(document ).ready(function(){
	var sCookie = document.cookie;
    if(sCookie.indexOf("cardapptoken") !== -1) {
    	sToken = sCookie.split("cardapptoken=")[1].split(";")[0];
    	$.ajax({
    		type: "POST",
    		url: "/getUserByToken",
    		data: sToken,
    		contentType: "application/json",
    		success: function(oUser) {
    			if(oUser) {
    				user = oUser;
    				setMyCards(oUser.id);
    				$("#userNameId").text(oUser.username);
    				$("#cash").text(oUser.money + "$");
    			} else {
    				window.location.href = "connexion.html";
    			}
    		}
    	});
    }else {
    	window.location.href = "connexion.html";
    }
});




function fillCurrentCard(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    //FILL THE CURRENT CARD
    $('#cardFamilyImgId')[0].src=imgUrlFamily;
    $('#cardFamilyNameId')[0].innerText=familyName;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price+"$";
};


function addCardToList(id,imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0'>\
            <div class='hidden content'>Sell</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";    
    $('#cardListId tr:last').after('<tr onclick="onRowclick(this)" id="'+id+'">'+content+'</tr>');    
};

function setMyCards(id){
	$.ajax({
		type: "GET",
		url: "/userCards/"+id,
		contentType: "application/json",
		success: function(oCards) {
			if(oCards.length > 0) {
				oCards.forEach(function(card){
					addCardToList(card.id,card.imgUrlFamily,card.family,card.imgUrl,card.name,card.description,card.hp,
							card.energy,card.attack,card.defence,card.price)
				});
				fillCurrentCard(oCards[0].imgUrlFamily,oCards[0].family,oCards[0].imgUrl,oCards[0].name
						,oCards[0].description,oCards[0].hp,oCards[0].energy
						,oCards[0].attack,oCards[0].defence,oCards[0].price);
				currentCardId = oCards[0].id;
			}
		}
	});
}

function onRowclick(row){
	fillCurrentCard("",row.cells[2].innerText,row.cells[0].firstElementChild.attributes.src
			,row.cells[0].children[1].innerText,row.cells[1].innerText
			,row.cells[3].innerText,row.cells[4].innerText,row.cells[6].innerText
			,row.cells[5].innerText,row.cells[7].innerText);
	currentCardId = row.getAttribute("id");
}

$("#sellButton").click(function(){
	var price = user.money + parseInt(this.children[1].innerText.split('$')[0]);
	
	sToken = document.cookie.split("cardapptoken=")[1].split(";")[0];
	$.ajax({
		type: "POST",
		url: "/user/updateMoney/"+price,
		data: sToken,
		contentType: "application/json",
		success: function() {
			window.location.reload(true);
		}
	});
	
	$.ajax({
		type: "POST",
		url: "/card/sellCard/"+currentCardId,
		data: sToken,
		contentType: "application/json",
		success: function() {
			window.location.reload(true);
		}
	});
	
});    
