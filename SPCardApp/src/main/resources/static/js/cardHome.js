var sToken;

$(document ).ready(function(){

    $("#playButtonId").click(function(){
        alert("Play button clicked ");
        //TO DO
    });    
    $("#buyButtonId").click(function(){
    	window.location.href = "cardBuyList.html";
    });    
    $("#sellButtonId").click(function(){
    	window.location.href = "cardSellList.html";
    });    
    
    var sCookie = document.cookie;
    if(sCookie.indexOf("cardapptoken") !== -1) {
    	sToken = sCookie.split("cardapptoken=")[1].split(";")[0];
    	$.ajax({
    		type: "POST",
    		url: "/getUserByToken",
    		data: sToken,
    		contentType: "application/json",
    		success: function(oUser) {
    			if(oUser) {
    				$("#userNameId").text(oUser.username);
    				$("#cashIcon").after(oUser.money + "$");
    			} else {
    				window.location.href = "connexion.html";
    			}
    		}
    	})
    } else {
    	window.location.href = "connexion.html";
    }
});

