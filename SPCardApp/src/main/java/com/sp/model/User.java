package com.sp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	private int id;
	private String username;
	private String password;
	private int money;
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<Card> cards = new ArrayList<>();
	private String token;
	
	public User(String username, String password, int money, String family, String token) {
		super();
		this.username = username;
		this.password = password;
		this.money = money;
		this.token = token;
		//this.cards = cards;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public void addCard(Card c){
		this.cards.add(c);
	}
	
	public void removeCard(Card c){
		this.cards.remove(c);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User() {
	}

	@Override
	public String toString() {
		return "User ["+this.username+"]: name:"+this.password;
	}

}
