package com.sp.rest;

import java.util.Date;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sp.repository.CardRepository;
import com.sp.service.CardService;
import com.sp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.sp.model.Card;
import com.sp.model.LoginForm;
import com.sp.model.User;
import com.sp.repository.UserRepository;
import com.sp.service.CardService;
import com.sp.service.UserService;

@RestController
public class UserRestController {

	@Autowired
    UserService uService;

	@Autowired
	CardService cService;

	@RequestMapping(method=RequestMethod.POST, value="/connexion")
	public String getUser(@RequestBody LoginForm login) {
		String token = "";
		User user = uService.findByUsernameAndPassword(login.getUsername(), login.getPassword());
		if(user != null) {
			token = user.getPassword() + user.getUsername() + new Date().getTime();
			user.setToken(token);
			uService.updateUser(user);
		}
		return token;
	}

	@RequestMapping(method=RequestMethod.POST, value="/getUserByToken")
	public User getUser(@RequestBody String token) {
		User user = uService.findByToken(token);
		return user;
	}

	@RequestMapping(method=RequestMethod.POST, value="/addUser")
	public User addUser(@RequestBody User user) {
		User userTest = uService.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		if(userTest != null) {
			System.out.println("user already created");
			return null;
		}

		for(int i=0;i<5;i++) {
			Card card = new Card("cardTest","ceci est description","7 familles","air", i, i, i, i, i, "", null);
			cService.addCard(card);
			user.addCard(card);
		}
		cService.addCard(new Card("Batman","Description de Batman","Plus beaucoup","Ombre", 150, 78, 15, 15, 250, "https://img.lemde.fr/2012/07/07/15/292.30846792291/465.35839543596/310.23363729625/688/0/60/0/ill_1729361_c0b3_201207071.0.988834193face_batman_ori.jpg", null));

		User createdUser = uService.updateUser(user);
		List<Card> cards = createdUser.getCards();
		for(Card card : cards) {
			card.setUser(createdUser);
			cService.addCard(card);
		}
		return createdUser;

	}
	
	@RequestMapping("/users")
	public List<User> listUsers() {
		return uService.getAllUsers();
	}

	@RequestMapping(method=RequestMethod.GET,value="/userCards/{id}")
	private List<Card> getAllUserCards(@PathVariable String id) {
		return uService.getAllUserCards(Integer.valueOf(id));
	}

	@RequestMapping(method=RequestMethod.POST, value="/user/addCard")
	private boolean addCard(@RequestBody String data) {
		JsonObject jo = new JsonParser().parse(data).getAsJsonObject();
		System.out.println(data);
		String token = jo.get("token").getAsString();

		System.out.println(token);
		String id_s = jo.get("id").getAsString();
/*		User u = uService.getUserByToken(token);
		System.out.println(token);
		System.out.println(id.toString());*/
		return uService.addCard(uService.getUserByToken(token), cService.getCard(Integer.valueOf(id_s)));
	}

	@RequestMapping(method=RequestMethod.POST, value="/user/updateMoney/{money}")
	private void updateMoney(@PathVariable Integer money, @RequestBody String token) {
		User user = uService.getUserByToken(token);
		user.setMoney(money);
		uService.updateUser(user);
	}
}
