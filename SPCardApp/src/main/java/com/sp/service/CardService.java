package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.sp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sp.model.Card;
import com.sp.model.User;
import com.sp.repository.CardRepository;


@Service
public class CardService {

	@Autowired
	CardRepository cRepository;

	@Autowired
	UserRepository uRepository;

	public void addCard(Card c) {
		Card createdCard = cRepository.save(c);
		System.out.println(createdCard);
	}

	public Card getCard(int id) {
		Optional<Card> hOpt =cRepository.findById(id);
		if (hOpt.isPresent()) {
			return hOpt.get();
		}else {
			return null;
		}

	}

	public List<Card> getAllCards() {
		return cRepository.findAll();
	}

	public List<Card> getAllForSaleCards() {
		List<Card> lc = cRepository.findAll();
		List<Card> liste_ok = new ArrayList<>();
		for(Card c : lc){
			if(c.getUser() == null){
				liste_ok.add(c);
			}
		}
		return liste_ok;
	}

	public void updateCard(Card card) {
		cRepository.save(card);
	}
}
