package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.List;
import java.util.Optional;

import com.sp.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.model.User;
import com.sp.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	UserRepository uRepository;

	@Autowired
	CardRepository cardRepository;

	public void addUser(User c) {
		User createdUser = uRepository.save(c);
		System.out.println(createdUser);
	}

	public User getUser(int id) {
		Optional<User> uOpt = uRepository.findById(id);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}

	}

	public User getUserByToken(String token) {
		return uRepository.findByToken(token);
		/*
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
		*/
	}
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		uRepository.findAll().forEach(users::add);
		return users;
	}
	
	public List<Card> getAllUserCards(int id) {
		return getUser(id).getCards();
	}


	public boolean addCard(User u, Card card) {
		if(card.getUser() == null) {
			u.addCard(card);
			u.setMoney(u.getMoney()-card.getPrice());
			card.setUser(u);
			uRepository.save(u);
			System.out.println("Card set to user");
			return true;
		}
		return false;
		/*
		List<Card> l = u.getCards();
		for(Card c: l){
			System.out.println(c);
		}

		 */
	}

	public User updateUser(User user) {
		return uRepository.save(user);
	}

	public User findByUsernameAndPassword(String username, String password){
		return uRepository.findByUsernameAndPassword(username, password);
	}

	public User findByToken(String token){
		return uRepository.findByToken(token);
	}
}
